import React from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import Personal from "./Personal/index";
import Projects from "./Projects/index";

import "./App.css";

const App = () => (
  <Router>
    <React.Fragment>
      <div className="nav"> 
        <div>
          <NavLink exact={true} to="/" activeClassName="active">
            Personal
          </NavLink>
        </div>
        <div>
          <NavLink to="/projects" activeClassName="active">
            Projects
          </NavLink>
        </div>
      </div>

      <div className="container">
        <Route exact path="/" component={Personal} />
        <Route path="/projects" component={Projects} />
      </div>
    </React.Fragment>
  </Router>
);

export default App;
