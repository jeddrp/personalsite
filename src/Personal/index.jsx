import React, { useEffect } from "react";
import anime from "animejs/lib/anime.es.js";

import "./styles.css";

export default () => {
  useEffect(() => {
    anime({
      targets: ".personalTitle, .facet",
      opacity: [0, 1],
      duration: 1000,
      easing: "linear",
    });
  });

  return (
    <div className="personal">
      <h3 className="personalTitle">What's important to me?</h3>
      <div className="personalContainer">
        <div className="facet strava">
          <div className="content">
            <h3 className="tagline">Health</h3>
            <ul>
              <li>
                <strong>Endurance Exercise →</strong> I run for fun! I've got several half marathons and 1 full marathon under my belt, and
                will be attempting a half-iron triathlon in June.
              </li>
              <li>
                <strong>Vegetarian →</strong> A New Year's challenge that has so
                far stuck.
              </li>
              <li>
                <strong>Total well being →</strong> Always looking for ways to optimize my physical, mental, and emotional health.
              </li>
            </ul>
            <a
              className="photoSource"
              target="_blank"
              href="https://www.strava.com/"
            >
              Courtesy
            </a>
          </div>
        </div>
        <div className="facet relationships">
          <div className="content">
            <h3 className="tagline">Relationships</h3>
            <ul>
              <li>
                <strong>Family →</strong> My family is wild and awesome. Outside of work, you'll find me with them.
              </li>
              <li>
                <strong>Work →</strong> The people make the job for me. It could be the least interesting work, but if the people are great, I'm happy.
              </li>
            </ul>
            <a
              className="photoSource"
              target="_blank"
              href="https://unsplash.com/photos/Sj0iMtq_Z4w"
            >
              Courtesy
            </a>
          </div>
        </div>
        <div className="facet creativity">
          <div className="content">
            <h3 className="tagline">Creating</h3>
            <ul>
              <li>
                <strong>YouTube →</strong> I used to run a channel with game development guides and it was very fulfilling.
              </li>
              <li>
                <strong>Music →</strong> Listening for now, making someday. I am slowly self teaching myself the piano.
              </li>
              <li>
                <strong>Game Development →</strong> I've only scratched the
                surface, but have so many worlds I want to bring to life.
              </li>
            </ul>
            <a
              className="photoSource"
              target="_blank"
              href="https://unsplash.com/photos/KcoR5PAa4c0"
            >
              Courtesy
            </a>
          </div>
        </div>
        <div className="facet gaming">
          <div className="content">
            <h3 className="tagline">Gaming</h3>
            <ul>
              <li>
                <strong>Rocket League →</strong> A classic for me, of which I am 1/5th the way to total mastery, if you know what I mean. 🙈
              </li>
              <li>
                <strong>Palworld →</strong> I'm riding the hype wave.
              </li>
              <li>
                <strong>Anything →</strong> Let's play!
              </li>
            </ul>
            <a
              className="photoSource"
              target="_blank"
              href="https://www.rocketleague.com/"
            >
              Courtesy
            </a>
          </div>
        </div>
        
      </div>
    </div>
  );
};
