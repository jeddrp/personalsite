import React, { useState } from "react";
import "./styles.css";

export default ({ title, info, img, source, sourceDescription }) => {
  const [gifActive, setGifActive] = useState(true);

  return (
    <div className="project">
      {title && <h3 className="projectHeader">{title}</h3>}
      <p className="projectInfo">{info}</p>
      {source && (
        <a className="sourceLink" target="_blank" href={source}>
          {sourceDescription}
        </a>
      )}
      {img && (
        <a
          href=""
          className="gifCollapse"
          onClick={(e) => {
            e.preventDefault();
            setGifActive((isActive) => !isActive);
          }}
        >
          {gifActive ? "Collapse" : "Expand"}
        </a>
      )}
      {img && <img src={img} hidden={!gifActive}></img>}
    </div>
  );
};
