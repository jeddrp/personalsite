import React, { useEffect } from "react";
import anime from "animejs/lib/anime.es.js";

import ropemanGif from "./images/ropeman.gif";
import cryptoPng from "./images/crypto.png";
import timewillowPng from "./images/timewillow.png";

import "./styles.css";

import Project from "./Project/index";

export default () => {
  const projects = [
    {
      title: "Timewillow",
      info: "I use time blocking to get to the most important tasks and stay focused. I was using an app that cost $20/mo, so as a stubborn engineer, I built and shipped my own solution. (If you check it out, please send me feedback! It has a long way to go. 💚)",
      img: timewillowPng,
      source: "https://timewillow.com",
      sourceDescription: "View Site",
    },
    {
      title: "Crypto Trading Bots",
      info: "My tax return shows a loss of $4, but that doesn't change that I had a blast writing algorithms and strategies for crypto trading. It's a nice change of pace from my day to day engineering tasks.",
      img: cryptoPng,
    },
    {
      title: "10 Minute Tutorials",
      info: "I created this YouTube channel to provide short and simple tutorials on what I learned while making games in the Unity Game Engine. I was thrilled with the community response and enjoyed teaching others.",
      source: "https://www.youtube.com/channel/UC2GATQDFEjlmfH3Ynvtge6Q",
      sourceDescription: "View Channel",
      img: ropemanGif,
    },
    {
      title: "(This Site)",
      source: "https://bitbucket.org/jeddrp/personalsite/src",
      sourceDescription: "Source Code",
      info: "This site is built in React, bootstrapped by the create-react-app npm package. It is hosted with a combination of Amazon's Cloudfront, S3, and Route 53 services.",
    },
  ];

  useEffect(() => {
    anime({
      targets: ".project",
      opacity: [0, 1],
      delay: anime.stagger(600),
      duration: 1000,
      easing: "linear",
    });
  });

  return (
    <div className="projects">
      {projects.map((project, index) => (
        <Project
          key={index}
          title={project.title}
          source={project.source}
          sourceDescription={project.sourceDescription}
          status={project.status}
          info={project.info}
          img={project.img}
        />
      ))}
    </div>
  );
};
